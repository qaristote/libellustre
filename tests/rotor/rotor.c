

/******* From ArduPilot *******/
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <unistd.h>

/* build ioctl unique identifiers for R/W operations */
#define PWM_MAGIC 'p'
typedef struct { unsigned int val[4]; } __attribute__ ((packed)) pwm_delos_quadruplet;
#define PWM_DELOS_SET_RATIOS _IOR(PWM_MAGIC, 9,  pwm_delos_quadruplet*)
#define PWM_DELOS_SET_SPEEDS _IOR(PWM_MAGIC, 10, pwm_delos_quadruplet*)
#define PWM_DELOS_SET_CTRL   _IOR(PWM_MAGIC, 11, unsigned int)
#define PWM_DELOS_REQUEST    _IO(PWM_MAGIC, 12)

#define PWM_NB_BITS   (9)

#define PWM_REG_RATIO_PRECISION_MASK (PWM_NB_BITS<<16)
#define PWM_REG_SATURATION (PWM_REG_RATIO_PRECISION_MASK|PWM_TOTAL_RANGE)

/* PWM can take value between 0 and 511 */
#define PWM_TOTAL_RANGE (1<<PWM_NB_BITS)

enum {
  SiP6_PWM0_START = (1<<0),
  SiP6_PWM1_START = (1<<1),
  SiP6_PWM2_START = (1<<2),
  SiP6_PWM3_START = (1<<3),
};

/********************/

struct rotor_state {
    int pwmfd;
    uint16_t min_pwm;
    uint16_t max_pwm;
    uint16_t freq_hz;
};

bool init(struct rotor_state *rs)
{
    rs->min_pwm = 1100;
    rs->max_pwm = 1900;
    rs->freq_hz = 50;

    rs->pwmfd = open("/dev/pwm", O_RDWR);
    if (rs->pwmfd < 0) return false;

    pwm_delos_quadruplet m = {{ 1, 1, 1, 1 }};
    int ret = ioctl(rs->pwmfd, PWM_DELOS_SET_SPEEDS, &m);
    printf("Return Speeds: %d\n", ret);

    unsigned int control_reg = (SiP6_PWM0_START|SiP6_PWM1_START|SiP6_PWM2_START|SiP6_PWM3_START);
    ret = ioctl(rs->pwmfd, PWM_DELOS_SET_CTRL, &control_reg);
    printf("Return control: %d\n", ret);

    return true;
}

int push(struct rotor_state *rs, uint16_t values[4])
{
    int i, ret;
    pwm_delos_quadruplet m;

    //for (int i=0;i<4;i++) rpm_ref[i] = _period_us_to_rpm(_period_us[i]);

    for (i = 0; i < 4; ++i) {
	if (values[i] <= (PWM_TOTAL_RANGE)) {
	    m.val[i] = values[i];
	} else {
	    m.val[i] = PWM_REG_SATURATION;
	}
	/* The upper 16-bit word of the ratio register contains the number
	* of bits used to code the ratio command  */
	m.val[i] |= PWM_REG_RATIO_PRECISION_MASK;
    }

    ret = ioctl(rs->pwmfd, PWM_DELOS_SET_RATIOS, &m);

    return ret;
}

int main(int argc, char* argv[])
{
    struct rotor_state rs;
    uint16_t values[4] = { 0, 0, 0, 0 };
    int i;

    init(&rs);
    usleep(50000);

    for (i = 0; i < PWM_TOTAL_RANGE; i += 10) {
	values[0] = i;
	values[1] = i;
	values[2] = i;
	values[3] = i;
	printf("%d %d %d %d\n", values[0], values[1], values[2], values[3]);
	push(&rs, values);
	usleep(50000);
    }

     for (i = PWM_TOTAL_RANGE; i > 0; i -= 10) {
	values[0] = i;
	values[1] = i;
	values[2] = i;
	values[3] = i;	
	printf("%d %d %d %d\n", values[0], values[1], values[2], values[3]);
	push(&rs, values);
	usleep(20000);
    }
     
    return 0;
}

