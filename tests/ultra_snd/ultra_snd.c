/* 20200107 T. Bourke
 *
 * Code for testing the ultrasonic sensing capabilities of the Parrot Mambo
 * drone.
 *
 * cc -o ultra_snd -Wall ultra_snd.c
 */

#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/errno.h>

#include <poll.h>

#include <libgen.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

// from https://github.com/parrot-opensource/mambo-opensource
// sources/linux-2.6.36/linux-2.6.36/drivers/parrot/ultra_sound/ultra_snd.h
#include "ultra_snd.h"

static char *me = 0;

#define ULTRA_SND "/dev/ultra_snd"

void eprintf(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    fprintf(stderr, "%s: ", me);
    vfprintf(stderr, format, ap);
    va_end(ap);
}

int main(int argc, char* argv[])
{
    int usnd;
    size_t buf_size = (1 << 17);
    uint8_t *buf = calloc(buf_size, 1);
    int num_pulses = 7;
    struct pollfd pollusnd = { .events = POLLIN };
    int i, r;

    me = basename(argv[0]);

    if (buf == NULL) {
	eprintf( "could not allocate buffer\n");
	return -1;
    }

    usnd = open(ULTRA_SND, O_RDWR);
    if (usnd == -1) {
	eprintf( "could not open %s (%d)\n", ULTRA_SND, errno);
	return errno;
    }
    pollusnd.fd = usnd;

    printf("# setting size\n");
    ioctl(usnd, USND_SETSIZE, &buf_size);

    printf("# sending %d pulses\n", num_pulses);
    ioctl(usnd, USND_PULSES, num_pulses);

    printf("# waiting for data\n");
    r = poll(&pollusnd, 1, 5000); // timeout = 5s

    if (r == 0) {
	printf("# timeout\n");
	// return -2;
    } else if (r < 0) {
	eprintf("poll failed (%d)\n", errno);
	return -3;
    }

    printf("# copying data\n");
    r = ioctl(usnd, USND_COPYSAMPLE, buf);
    // alternative: try the mmap interface

    if (r < 0) {
	eprintf("ioctl failed (%d)\n", errno);
	return -4;
    }

    for (i = 0; i < buf_size; ++i) {
	printf("%d\t%u\n", i, (unsigned int)buf[i]);
    }

    return 0;
}

