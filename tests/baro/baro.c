#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/errno.h>

typedef struct Baro_input_event {
  float pressure ;
  int fd ;
  bool data_avail ;
} * p_Baro_input_event ;

bool init(p_Baro_input_event bie) {
  bie->data_avail = false ;
  bie->fd = open("/dev/input/baro_event", O_RDONLY) ;
  printf("%d \n", bie->fd) ;
  return true ;
}

void update(p_Baro_input_event bie) {
  if (bie->data_avail) {
    bie->data_avail = false ;
    printf("Pressure : %f \n", bie->pressure) ;
    fflush(stdout) ;
      }
}

void tick(p_Baro_input_event bie) {
  struct input_event ev ;
  ssize_t n ;
  n = read(bie->fd, &ev, sizeof(ev)) ;
  if (n == sizeof(ev) && ev.type == EV_ABS && ev.code == ABS_PRESSURE) {
    bie->pressure = 100.f * ((float) ev.value) / 4096.f ;
    bie->data_avail = true ;
  }
}

void tick_test(p_Baro_input_event bie) {
  struct input_event ev ;
  ssize_t n ;
  n = read(bie->fd, &ev, sizeof(ev)) ;
  printf("%d (%d) - %d - %d \n", n, sizeof(ev), errno, bie->fd) ;
  if (true) {
    printf("%ld : %d, %d, %f \n", (long int) ev.time.tv_usec, (int) ev.type, (int) ev.code, (float) ev.value) ;
  }
} 

int main() {
  struct Baro_input_event bie ;
  init(&bie) ;
  while (true) {
    tick(&bie) ;
    update(&bie) ;
  }
  return 0 ; 
}
