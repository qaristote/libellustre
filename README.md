# LibelLustre

LibelLustre is a project aiming to use the advantages of the Lustre synchronous programming language to develop a controller for the Parrot Mambo. It was supervised by [Timothy Bourke](https://www.tbrk.org/).

# Warning

This code is highly experimental. Use it at your own risk.

# Building 
## Prerequisites

1. Download the the ARM GCC cross-compiler for your architecture, untar it and aadd it to your `PATH`. For example, for Intel CPUs, such a compiler can be found [here](https://sourcery.mentor.com/GNUToolchain/package12813/public/arm-none-linux-gnueabi/arm-2014.05-29-arm-none-linux-gnueabi-i686-pc-linux-gnu.tar.bz2).

## Flashing files

To flash the Parrot Mambo with a file, proceed as such :
1. Connect the Parrot Mambo via USB to your computer and boot it up.
2. Mount the `Parrot_Mini` drive.
3. Copy the software you want to flash (a `.plf` file) at the root of the drive (i.e. in `Parrot_Mini/`).
4. Wait for the file transfer to complete. While the file is transferring, the eyes of the Parrot Mambo will blink.
5. Once the transfer is complete, the right-eye light should be green, and the left-eye one orange.
6. Unmount the drive and unplug the Parrot Mambo from your computer.
7. Installation begins. The lights alternatively flash orange with increasing rapidity. Do not remove the battery while the update installation is in progress.

## Communicating with the Parrot Mambo via bluetooth

1. Flash the Parrot Mambo with Parrot's [Education Firmware](https://github.com/Parrot-Developers/MinidronesEdu).
2. Connect the Parrot Mambo via USB to your computer and boot it up.
3. Using `ip a`, you should notice that a new network interface has appeared.
4. Tell your system about this network interface by putting the following in `/etc/network/interfaces.d/parrot_mambo.iface` :
```
iface <your-interface> inet static
      address 192.168.2.3
      netmask 255.255.255.0
```
5. Start the Ethernet through USB connection with your Parrot Mambo with 
```
sudo ifup <your-interface>
```
6. You should now be able to connect to the Parrot Mambo with telnet using
```
telnet 192.168.2.1
```
7. While connected to the Parrot Mambo with telnet, enable the Bluetooth interface with
```
kk
pstop delosd
pstop ledd
pstart bleproxy
ifconfig bnep 192.168.3.1 netmask 255.255.255.0 up
```
8. Follow the [instructions](https://github.com/Parrot-Developers/MinidronesEdu/blob/master/mambo-2.6.11/README.bluetooth.md) from Parrot to connect your computer to the Parrot Mambo via bluetooth. You should now be able to use ftp and telnet with your Parrot Mambo through Bluetooth.

## Building and running the controller
1. Write your controller code in `controller/controller.ept`.
2. Use `make` to build the controller, and `make clean` if you want to remove the generated files.
3. Transfer the controller to the Parrot Mambo (via Bluetooth here) with
```
ftp 192.168.3.1
ftp> put controller.bin controller.bin
```
4. To run the controller, connect to the Parrot Mambo with telnet and execute the following commands 
```
cd /data/edu
chmod u+x controller.bin
./controller.bin
```
