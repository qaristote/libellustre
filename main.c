#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "controller.h"
#include "iface/iface.h"

Controller__controller_mem controller_state ;

int main(int argc, char* argv[]) {
  Controller__controller_reset(&controller_state) ;

  struct timespec t_sensors, t_sleep ; // t_step, t_actuators
  int sleep_time ;

  baro baro = baro_init() ; // pressure
  mpu6050 mpu6050 = mpu6050_init() ; // accel & gyro
  rotors rotors = rotors_init() ; // rotors

  Controller__controller_out result ;
  
  while (1) {
    clock_gettime(CLOCK_REALTIME, &t_sensors) ;
    baro_update(baro) ;
    mpu6050_update(mpu6050) ;
    
    // clock_gettime(CLOCK_REALTIME, &t_step) ;
    Controller__controller_step(mpu6050->accel_x, mpu6050->accel_y, mpu6050->accel_z,
				mpu6050->gyro_x, mpu6050->gyro_y, mpu6050->gyro_z,
				&result, &controller_state) ;

    // clock_gettime(CLOCK_REALTIME, &t_actuators) ;
    rotors_update(rotors,
		  result.front_right,
		  result.back_right,
		  result.front_left,
		  result.back_left) ;

    clock_gettime(CLOCK_REALTIME, &t_sleep) ;
    sleep_time = (100000000 - (t_sleep.tv_nsec - t_sensors.tv_nsec)) / 1000 ;
    usleep(sleep_time) ;
  }
  
  return 0 ;
}

    
