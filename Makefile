CC = arm-none-linux-gnueabi-gcc
HEPTC = heptc -O
CFLAGS = -lrt

HEPT_INCLUDE:=$(shell $(HEPTC) -where)/c

OUT = build
CONTROLLER_C = $(OUT)/controller_c

IFACE = baro.o rotors.o mpu6050.o

all : $(OUT) controller.bin

controller.bin : main.o $(IFACE) controller.o
	cd $(OUT) ; $(CC) -o ../$@ -Wall $^ $(CFLAGS)

main.o : main.c $(CONTROLLER_C)/controller.h iface/iface.h
	$(CC) -c -o $(OUT)/$@ -Wall -I $(CONTROLLER_C) -I $(HEPT_INCLUDE) $< $(CFLAGS)

$(IFACE) : %.o : iface/%.c iface/iface.h
	$(CC) -c -o $(OUT)/$@ -Wall $< $(CFLAGS)

controller.o : $(CONTROLLER_C)/controller.c $(CONTROLLER_C)/controller.h
	$(CC) -c -o $(OUT)/$@ -Wall -I $(HEPT_INCLUDE) $< $(CFLAGS)
$(CONTROLLER_C)/controller.h : $(CONTROLLER_C)/controller.c
$(CONTROLLER_C)/controller.c : controller/controller.ept
	cd $(OUT) ; $(HEPTC) -target c ../$<

$(OUT) :
	mkdir -p $(OUT)

.PHONY : clean
clean :
	rm -r build
	rm controller.bin
