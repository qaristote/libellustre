/******* From ArduPilot *******/
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include "iface.h"

/* build ioctl unique identifiers for R/W operations */
#define PWM_MAGIC 'p'
typedef struct { unsigned int val[4]; } __attribute__ ((packed)) pwm_delos_quadruplet;
#define PWM_DELOS_SET_RATIOS _IOR(PWM_MAGIC, 9,  pwm_delos_quadruplet*)
#define PWM_DELOS_SET_SPEEDS _IOR(PWM_MAGIC, 10, pwm_delos_quadruplet*)
#define PWM_DELOS_SET_CTRL   _IOR(PWM_MAGIC, 11, unsigned int)
#define PWM_DELOS_REQUEST    _IO(PWM_MAGIC, 12)

#define PWM_NB_BITS   (9)

#define PWM_REG_RATIO_PRECISION_MASK (PWM_NB_BITS<<16)
#define PWM_REG_SATURATION (PWM_REG_RATIO_PRECISION_MASK|PWM_TOTAL_RANGE)

/* PWM can take value between 0 and 511 */
#define PWM_TOTAL_RANGE (1<<PWM_NB_BITS)

enum {
  SiP6_PWM0_START = (1<<0),
  SiP6_PWM1_START = (1<<1),
  SiP6_PWM2_START = (1<<2),
  SiP6_PWM3_START = (1<<3),
};

/********************/


rotors rotors_init() {

  rotors rotors = malloc(sizeof(*rotors));
   
  rotors->min_pwm = 1100;
  rotors->max_pwm = 1900;
  rotors->freq_hz = 50;

  rotors->pwmfd = open("/dev/pwm", O_RDWR);
  if (rotors->pwmfd < 0) return false;

  pwm_delos_quadruplet m = {{ 1, 1, 1, 1 }};
  ioctl(rotors->pwmfd, PWM_DELOS_SET_SPEEDS, &m);
  unsigned int control_reg = (SiP6_PWM0_START|SiP6_PWM1_START|SiP6_PWM2_START|SiP6_PWM3_START);
  ioctl(rotors->pwmfd, PWM_DELOS_SET_CTRL, &control_reg);

  return rotors ;
}

int rotors_update(rotors rotors,
		  uint16_t front_right,
		  uint16_t back_right,
		  uint16_t front_left,
		  uint16_t back_left) {
  int ret;
  pwm_delos_quadruplet m;

  if (front_right <= (PWM_TOTAL_RANGE)) {
    m.val[0] = front_right;
  } else {
    m.val[0] = PWM_REG_SATURATION;
  }
  /* The upper 16-bit word of the ratio register contains the number
   * of bits used to code the ratio command  */
  m.val[0] |= PWM_REG_RATIO_PRECISION_MASK;

  if (back_right <= (PWM_TOTAL_RANGE)) {
    m.val[1] = back_right;
  } else {
    m.val[1] = PWM_REG_SATURATION;
  }
  /* The upper 16-bit word of the ratio register contains the number
   * of bits used to code the ratio command  */
  m.val[1] |= PWM_REG_RATIO_PRECISION_MASK;
    
  if (front_left <= (PWM_TOTAL_RANGE)) {
    m.val[2] = front_left;
  } else {
    m.val[2] = PWM_REG_SATURATION;
  }
  /* The upper 16-bit word of the ratio register contains the number
   * of bits used to code the ratio command  */
  m.val[2] |= PWM_REG_RATIO_PRECISION_MASK;
      
  if (back_left <= (PWM_TOTAL_RANGE)) {
    m.val[3] = back_left;
  } else {
    m.val[3] = PWM_REG_SATURATION;
  }
  /* The upper 16-bit word of the ratio register contains the number
   * of bits used to code the ratio command  */
  m.val[3] |= PWM_REG_RATIO_PRECISION_MASK;
  
  ret = ioctl(rotors->pwmfd, PWM_DELOS_SET_RATIOS, &m);

  return ret;
}
