#include <stdint.h>
#include <stdbool.h>

typedef struct baro {
  float pressure ;
  int fd ;
} * baro ;
baro baro_init() ;
void baro_update(baro baro) ;

typedef struct mpu6050 {
  bool data_ready ;
  int accel_x ;
  int accel_y ;
  int accel_z ;
  int gyro_x ;
  int gyro_y ;
  int gyro_z ;
  int temp;
  int i2c ;
} * mpu6050;
mpu6050 mpu6050_init() ;
void mpu6050_update(mpu6050 mpu6050) ;
  
typedef struct rotors {
  int pwmfd;
  uint16_t min_pwm;
  uint16_t max_pwm;
  uint16_t freq_hz;
} * rotors ;
rotors rotors_init() ;
int rotors_update(rotors rotors,
		  uint16_t front_right,
		  uint16_t back_right,
		  uint16_t front_left,
		  uint16_t back_left) ;
