/* 20200106 T. Bourke
 * 20200114 Q. Aristote
 *
 * Simple polling of MPU-6050 on I2C bus.
 * (does not use DMP)
 *
 * cc -o mpu6050 -Wall mpu6050.c
 */

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/errno.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include <unistd.h>
#include <libgen.h>

#include "iface.h"

#define I2C_ADAPTER "/dev/i2c-0"

#define MPU6050_SLA 0x68

// see: "MPU-6000 and MPU-6050 Register Map and Descriptions - revision 4.2"
#define MPU6050_SMPRT_DIV         0x19
#define MPU6050_CONFIG            0x1a

#define MPU6050_DLPF_CFG_A260_G256 0
#define MPU6050_DLPF_CFG_A184_G188 1
#define MPU6050_DLPF_CFG_A94_G98   2
#define MPU6050_DLPF_CFG_A44_G42   3
#define MPU6050_DLPF_CFG_A21_G20   4
#define MPU6050_DLPF_CFG_A10_G10   5
#define MPU6050_DLPF_CFG_A5_G5     6

#define MPU6050_GYRO_CONFIG       0x1b

#define MPU6050_FS_SEL_250DPS     0
#define MPU6050_FS_SEL_500DPS     1
#define MPU6050_FS_SEL_1000DPS    2
#define MPU6050_FS_SEL_2000DPS    3

#define MPU6050_ACCEL_CONFIG      0x1c

#define MPU6050_AFS_SEL_2G        0
#define MPU6050_AFS_SEL_4G        1
#define MPU6050_AFS_SEL_8G        2
#define MPU6050_AFS_SEL_16G       3

#define MPU6050_FIFO_EN           0x23
#define MPU6050_INT_ENABLE        0x38
#define MPU6050_INT_STATUS        0x3a
#define MPU6050_ACCEL_XOUT_H      0x3b
#define MPU6050_ACCEL_XOUT_L      0x3c
#define MPU6050_ACCEL_YOUT_H      0x3d
#define MPU6050_ACCEL_YOUT_L      0x3e
#define MPU6050_ACCEL_ZOUT_H      0x3f
#define MPU6050_ACCEL_ZOUT_L      0x40
#define MPU6050_TEMP_OUT_H        0x41
#define MPU6050_TEMP_OUT_L        0x42
#define MPU6050_GYRO_XOUT_H       0x43
#define MPU6050_GYRO_XOUT_L       0x44
#define MPU6050_GYRO_YOUT_H       0x45
#define MPU6050_GYRO_YOUT_L       0x46
#define MPU6050_GYRO_ZOUT_H       0x47
#define MPU6050_GYRO_ZOUT_L       0x48
#define MPU6050_SIGNAL_PATH_RESET 0x68
#define MPU6050_USER_CTRL         0x6a
#define MPU6050_PWR_MGMT_1        0x6b
#define MPU6050_PWR_MGMT_2        0x6c
#define MPU6050_FIFO_COUNT_H      0x72
#define MPU6050_FIFO_COUNT_L      0x73
#define MPU6050_FIFO_R_W          0x74
#define MPU6050_WHO_AM_I          0x75


bool i2c_write_reg(int i2c, uint8_t reg, uint8_t v)
{
  char buf[2];
  int r;

  buf[0] = reg;
  buf[1] = v;

  r = write(i2c, buf, 2);

  return (r == 2);
}

// v must have space for count bytes
bool i2c_read_regs(int i2c, uint8_t sla,
		   uint8_t start_reg, int count, uint8_t *v)
{
  bool r;
  struct i2c_msg msgs[] = {
			   // first message: write the register address to the slave
			   { .addr  = sla,
			     .flags = 0,
			     .len   = 1,
			     .buf   = v, },
			   // second message: read the slave response into v
			   { .addr  = sla,
			     .flags = I2C_M_RD,
			     .len   = count,
			     .buf   = v, },
  };
  struct i2c_rdwr_ioctl_data io_msgs = {
					.msgs  = msgs,
					.nmsgs = 2,
  };

  if (count < 1) return 0;

  v[0] = start_reg;

  r = (ioctl(i2c, I2C_RDWR, &io_msgs) == 2);

  return r;
}

mpu6050 mpu6050_init() {
  mpu6050 mpu6050 = malloc(sizeof(*mpu6050)) ;

  mpu6050->i2c = open(I2C_ADAPTER, O_RDWR) ;
  if (mpu6050->i2c == -1) {
    goto error ;
  }
  
  ioctl(mpu6050->i2c, I2C_SLAVE, MPU6050_SLA);

  // clear sleep bit and set clock source to gyro-x
  if (!i2c_write_reg(mpu6050->i2c, MPU6050_PWR_MGMT_1, 0x01)) return 0; 
  usleep(100);

  // set Digital Low-Pass Filter bandwidths with sample rate = 10 Hz
  if (!i2c_write_reg(mpu6050->i2c, MPU6050_CONFIG, MPU6050_DLPF_CFG_A44_G42))
    goto error; 
  if (!i2c_write_reg(mpu6050->i2c, MPU6050_SMPRT_DIV, (uint8_t)100))
    goto error; 

  if (!i2c_write_reg(mpu6050->i2c, MPU6050_GYRO_CONFIG, MPU6050_FS_SEL_2000DPS << 3))
    goto error; 
  if (!i2c_write_reg(mpu6050->i2c, MPU6050_ACCEL_CONFIG, MPU6050_AFS_SEL_16G << 3))
    goto error; 

  return mpu6050 ;
  
 error :
  free(mpu6050) ;
  return NULL ;
}

void mpu6050_update(mpu6050 mpu6050) {
  uint8_t buf[(MPU6050_GYRO_ZOUT_L - MPU6050_INT_STATUS) + 1];
  
  if (i2c_read_regs(mpu6050->i2c, MPU6050_SLA, MPU6050_INT_STATUS,
		    (MPU6050_GYRO_ZOUT_L - MPU6050_INT_STATUS) + 1, buf)) {
    mpu6050->data_ready = (buf[0] & 0x01);

    mpu6050->accel_x = (int8_t)((buf[1] << 8) | (uint8_t)buf[2]);
    mpu6050->accel_y = (int8_t)((buf[3] << 8) | (uint8_t)buf[4]);
    mpu6050->accel_z = (int8_t)((buf[5] << 8) | (uint8_t)buf[6]);

    mpu6050->temp = (int8_t)((buf[7] << 8) | (uint8_t)buf[8]);

    mpu6050->gyro_x = (int8_t)((buf[ 9] << 8) | (uint8_t)buf[10]);
    mpu6050->gyro_y = (int8_t)((buf[11] << 8) | (uint8_t)buf[12]);
    mpu6050->gyro_z = (int8_t)((buf[13] << 8) | (uint8_t)buf[14]);
  }
}
