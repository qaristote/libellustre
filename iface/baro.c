#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/errno.h>
#include "iface.h"

baro baro_init() {
  baro baro = malloc(sizeof(*baro)) ;
  baro->fd = open("/dev/input/baro_event", O_RDONLY) ;
  baro->pressure = 0 ;
  return baro ;
}

void baro_update(baro baro) {
  struct input_event ev ;
  ssize_t n ;
  n = read(baro->fd, &ev, sizeof(ev)) ;
  if (n == sizeof(ev) && ev.type == EV_ABS && ev.code == ABS_PRESSURE) {
    baro->pressure = 100.f * ((float) ev.value) / 4096.f ;
  }
}
